package ictgradschool.industry.sorting;

/**
 * Implementation of Merge Sort to sort an array of integers.
 */
public class MergeSort implements ISortAlgorithm {

    @Override
    public void sort(int[] numbers) {

        // Divide the array into two roughly equal sections
        int[][] divided = divide(numbers);

        // Sort each half that has more than one element (arrays with one element are sorted by definition).
        if (divided[0].length > 1) {
            sort(divided[0]);
        }
        if (divided[1].length > 1) {
            sort(divided[1]);
        }

        // Merge the two sorted sub-arrays back into the result array
        merge(divided[0], divided[1], numbers);

    }

    /**
     * Splits the given array in half.
     *
     * @param numbers the array to split
     * @return two int arrays
     */
    private int[][] divide(int[] numbers) {

        int midpoint = numbers.length / 2;

        int[] first = new int[midpoint];
        System.arraycopy(numbers, 0, first, 0, first.length);

        int[] second = new int[numbers.length - midpoint];
        System.arraycopy(numbers, midpoint, second, 0, second.length);

        return new int[][]{first, second};
    }

    /**
     * Merges two sorted int arrays into a single sorted int array.
     *
     * @param numbers1 a sorted int array
     * @param numbers2 a sorted int array
     * @param result   an int array where the result will be stored
     */
    private void merge(int[] numbers1, int[] numbers2, int[] result) {

        int expectedRL = numbers1.length + numbers2.length;
        if (expectedRL != result.length) {
            throw new RuntimeException("Expected result length was " + expectedRL + " but was " + result.length);
        }

        int i1 = 0, i2 = 0;
        for (int resultIndex = 0; resultIndex < result.length; resultIndex++) {

            if (i1 == numbers1.length) {

                result[resultIndex] = numbers2[i2++];

            } else if (i2 == numbers2.length) {

                result[resultIndex] = numbers1[i1++];

            } else if (numbers1[i1] < numbers2[i2]) {

                result[resultIndex] = numbers1[i1++];

            } else {

                result[resultIndex] = numbers2[i2++];
            }

        }

    }
}
