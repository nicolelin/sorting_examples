package ictgradschool.industry.sorting;

import java.util.Arrays;

/**
 * Implementation of Bubble Sort to sort an array of integers.
 * <p>Uncomment the commented lines to view the intermediate output after each pass.</p>
 */
public class BubbleSort implements ISortAlgorithm {

    @Override
    public void sort(int[] numbers) {
//        int pass = 0;
        for(int i = numbers.length - 1; i > 0; i--){
//            pass++;
//            int comp = 0;
            int swap = 0;
            for(int j = 0; j < i; j++){
                if(numbers[j] > numbers[j + 1]){
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                    swap++;
                }
//                comp++;
            }
//            System.out.print(Arrays.toString(numbers));
//            System.out.println(" Pass " + pass + "(" + comp + " comp, " + swap + " swap)");
            if(swap == 0){ //Quick bubble sort option
                return;
            }
        }
    }
}
