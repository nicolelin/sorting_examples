package ictgradschool.industry.sorting;

/**
 * Represents a sorting algorithm.
 */
public interface ISortAlgorithm {

    void sort(int[] numbers);

}
