package ictgradschool.industry.sorting;

import java.util.Arrays;

/**
 * Implementation of Selection Sort to sort an array of integers.
 * <p>Uncomment the commented lines to view the intermediate output after each pass.</p>
 */
public class SelectionSort implements ISortAlgorithm {
    @Override
    public void sort(int[] numbers) {
//        int pass = 0;
        for(int i = numbers.length - 1; i > 0; i--){
//            pass++;
//            int comp = 0;
//            int swap = 0;
            int max_pos = 0;
            for(int j = 1; j <= i; j++){
                if(numbers[j] > numbers[max_pos]){
                    max_pos = j;
                }
//                comp++;
            }
            if (max_pos != i){
                int temp = numbers[max_pos];
                numbers[max_pos] = numbers[i];
                numbers[i] = temp;
//                swap++;
            }
//            System.out.print(Arrays.toString(numbers));
//            System.out.println(" Pass " + pass + "(" + comp + " comp, " + swap + " swap)");
        }
    }
}
